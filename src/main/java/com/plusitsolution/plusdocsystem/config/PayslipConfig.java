package com.plusitsolution.plusdocsystem.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("file:${payslip.config.path}")
public class PayslipConfig {
	// payslip.position
	@Value("${payslip.position.payslipdate}")
	private String paySlipDate;

	@Value("${payslip.position.employeename}")
	private String employeeName;

	@Value("${payslip.position.employeecode}")
	private String employeeCode;

	@Value("${payslip.position.salary}")
	private String salary;

	@Value("${payslip.position.medicalfee}")
	private String medicalFee;

	@Value("${payslip.position.textotherincomewithtax}")
	private String textotherincomewithtax;

	@Value("${payslip.position.otherincomewithtax}")
	private String otherIncomeWithTax;

	@Value("${payslip.position.textotherincomewithouttax}")
	private String textotherincomewithouttax;

	@Value("${payslip.position.otherincomewithouttax}")
	private String otherIncomeWithoutTax;

	@Value("${payslip.position.incometax}")
	private String incomeTax;

	@Value("${payslip.position.socialsecurity}")
	private String socialSecurity;

	@Value("${payslip.position.otherdeduct}")
	private String otherDeduct;

	@Value("${payslip.position.textotherdeduct}")
	private String textotherdeduct;

	// payslip.value
	@Value("${payslip.value.textotherincomewithtax}")
	private String valuetextotherincomewithtax;

	@Value("${payslip.value.textotherincomewithouttax}")
	private String valuetextotherincomewithouttax;

	@Value("${payslip.value.textotherdeduct}")
	private String valuetextotherdeduct;

	// readfig.salaryxlsx.positioncolumn
	@Value("${salary.position.employeename}")
	private String readEmployeeName;

	@Value("${salary.position.employeecode}")
	private String readEmployeeCode;

	@Value("${salary.position.salary}")
	private String readSalary;

	@Value("${salary.position.medicalfee}")
	private String readMedicalfee;

	@Value("${salary.position.otherincomewithtax}")
	private String readOtherIncomeWithTax;

	@Value("${salary.position.otherincomewithouttax}")
	private String readOtherIncomeWithoutTax;

	@Value("${salary.position.incometax}")
	private String readIncomeTax;

	@Value("${salary.position.socialsecurity}")
	private String readSocialSecurity;

	@Value("${salary.position.otherdeduct}")
	private String readOtherDeduct;

	public String getReadEmployeeName() {
		return readEmployeeName;
	}

	public String getReadEmployeeCode() {
		return readEmployeeCode;
	}

	public String getReadSalary() {
		return readSalary;
	}

	public String getReadMedicalfee() {
		return readMedicalfee;
	}

	public String getReadOtherIncomeWithTax() {
		return readOtherIncomeWithTax;
	}

	public String getReadOtherIncomeWithoutTax() {
		return readOtherIncomeWithoutTax;
	}

	public String getReadIncomeTax() {
		return readIncomeTax;
	}

	public String getReadSocialSecurity() {
		return readSocialSecurity;
	}

	public String getReadOtherDeduct() {
		return readOtherDeduct;
	}

	public String getTextotherdeduct() {
		return textotherdeduct;
	}

	public String getValuetextotherdeduct() {
		return valuetextotherdeduct;
	}

	public String getTextotherincomewithtax() {
		return textotherincomewithtax;
	}

	public String getTextotherincomewithouttax() {
		return textotherincomewithouttax;
	}

	public String getValuetextotherincomewithtax() {
		return valuetextotherincomewithtax;
	}

	public String getValuetextotherincomewithouttax() {
		return valuetextotherincomewithouttax;
	}

	public String getPaySlipDate() {
		return paySlipDate;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public String getSalary() {
		return salary;
	}

	public String getMedicalFee() {
		return medicalFee;
	}

	public String getOtherIncomeWithTax() {
		return otherIncomeWithTax;
	}

	public String getOtherIncomeWithoutTax() {
		return otherIncomeWithoutTax;
	}

	public String getIncomeTax() {
		return incomeTax;
	}

	public String getSocialSecurity() {
		return socialSecurity;
	}

	public String getOtherDeduct() {
		return otherDeduct;
	}

}

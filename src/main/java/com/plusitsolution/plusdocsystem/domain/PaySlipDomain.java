package com.plusitsolution.plusdocsystem.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PaySlipDomain {
	private LocalDate date;
	private String employeeName;
	private String employeeCode;
	private Double salary;
	private Double medicalFee;
	private Double otherIncomeWithTax;
	private Double otherIncomeWithoutTax;
	private Double incomeTax;
	private Double socialSecurity;
	private Double otherDeduct;

	public String getFormatDate() {
		return (getDate().format(DateTimeFormatter.ofPattern("dd-MMM-yyyy"))).toUpperCase();
	}

	public String getFileName() {
		return (employeeCode + getDate().format(DateTimeFormatter.ofPattern("_MMM_yy"))).toUpperCase();
	}

	public String getFolderName() {
		return (getDate().format(DateTimeFormatter.ofPattern("yyyy_MM"))).toUpperCase();
	}

	public LocalDate getDate() {

		if (date == null) {
			LocalDate mydate = LocalDate.now();
			date = mydate.minusMonths(1).withDayOfMonth(25);
		}
		return date;
	}

	public Double getOtherIncomeWithTax() {
		if (otherIncomeWithTax == null) {
			otherIncomeWithTax = 0d;
		}
		return otherIncomeWithTax;
	}

	public void setOtherIncomeWithTax(Double otherIncomeWithTax) {
		this.otherIncomeWithTax = otherIncomeWithTax;
	}

	public Double getOtherIncomeWithoutTax() {
		if (otherIncomeWithoutTax == null) {
			otherIncomeWithoutTax = 0d;
		}
		return otherIncomeWithoutTax;
	}

	public void setOtherIncomeWithoutTax(Double otherIncomeWithoutTax) {
		this.otherIncomeWithoutTax = otherIncomeWithoutTax;
	}

	public Double getOtherDeduct() {
		if (otherDeduct == null) {
			otherDeduct = 0d;
		}
		return otherDeduct;
	}

	public void setOtherDeduct(Double otherDeduct) {
		this.otherDeduct = otherDeduct;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public Double getSalary() {
		if (salary == null) {
			salary = 0d;
		}
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public Double getMedicalFee() {
		if (medicalFee == null) {
			medicalFee = 0d;
		}
		return medicalFee;
	}

	public void setMedicalFee(Double medicalFee) {
		this.medicalFee = medicalFee;
	}

	public Double getIncomeTax() {
		if (incomeTax == null) {
			incomeTax = 0d;
		}
		return incomeTax;
	}

	public void setIncomeTax(Double incomeTax) {
		this.incomeTax = incomeTax;
	}

	public Double getSocialSecurity() {
		if (socialSecurity == null) {
			socialSecurity = 0d;
		}
		return socialSecurity;
	}

	public void setSocialSecurity(Double socialSecurity) {
		this.socialSecurity = socialSecurity;
	}
}

package com.plusitsolution.plusdocsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.plusitsolution.plusdocsystem.domain.UploadFilesDomain;
import com.plusitsolution.plusdocsystem.service.PaySlipService;

@RestController
public class PaySlipController {
	@Autowired
	private PaySlipService service;

	@PostMapping(path = "/convertPaySlipToPDF", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public void convertPaySlipToPDF(@ModelAttribute UploadFilesDomain domain) throws Exception {
		service.convertPaySlipToPDF(domain);
	}

}

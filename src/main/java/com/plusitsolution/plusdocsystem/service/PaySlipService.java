package com.plusitsolution.plusdocsystem.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.plusitsolution.common.toolkit.PlusExcelUtils;
import com.plusitsolution.common.toolkit.PlusFileUtils;
import com.plusitsolution.plusdocsystem.config.PayslipConfig;
import com.plusitsolution.plusdocsystem.domain.UploadFilesDomain;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Service
public class PaySlipService {
	@Autowired
	private PayslipConfig configXLSXPayslip;

	private String outputPath = System.getProperty("user.home") + "/Desktop/";

	private HashMap<String, Object> MAP_PAYSLIP_PDF = new HashMap<>();

	public HashMap<String, Object> readPaySlipPutToMap(Sheet sheetPaySlip) throws Exception {

		URL logo = ClassLoader.getSystemResource("images/logo.png");
		MAP_PAYSLIP_PDF.put("logo", Files.readAllBytes(Path.of(logo.toURI())));
		URL signature = ClassLoader.getSystemResource("images/signature.jpg");
		MAP_PAYSLIP_PDF.put("signature", Files.readAllBytes(Path.of(signature.toURI())));
		MAP_PAYSLIP_PDF.put("companyAddress", PlusExcelUtils.readCellAsString(sheetPaySlip, "L3")); // Company Address
		MAP_PAYSLIP_PDF.put("taxIDNumber", PlusExcelUtils.readCellAsString(sheetPaySlip, "A7")); // Tax ID Number
		MAP_PAYSLIP_PDF.put("date", PlusExcelUtils.readCellAsString(sheetPaySlip, "C8")); // Date
		MAP_PAYSLIP_PDF.put("employeeName", PlusExcelUtils.readCellAsString(sheetPaySlip, "C9")); // EmName
		MAP_PAYSLIP_PDF.put("employeeCode", PlusExcelUtils.readCellAsString(sheetPaySlip, "C10")); // EmCode

		if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F13") == 0.0) {// Salary
			MAP_PAYSLIP_PDF.put("salary", "-");
		} else {
			MAP_PAYSLIP_PDF.put("salary", String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F13")));
		}
		if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F14") == 0.0) {// Medical Fee
			MAP_PAYSLIP_PDF.put("medicalFee", "-");
		} else {
			MAP_PAYSLIP_PDF.put("medicalFee",
					String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F14")));
		}
		if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F15") == 0.0) {// OT
			MAP_PAYSLIP_PDF.put("OT", "-");
		} else {
			MAP_PAYSLIP_PDF.put("OT", String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F15")));
		}
		if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "D15") == 0.0) { // Date OT
			MAP_PAYSLIP_PDF.put("dateOT", "-");
		} else {
			MAP_PAYSLIP_PDF.put("dateOT", String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "D15")));
		}
		if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F18") == 0.0) {// Income
			MAP_PAYSLIP_PDF.put("income", "-");
		} else {
			MAP_PAYSLIP_PDF.put("income", String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F18")));
		}
		// -- Income --
		// -- Deduction --
		if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L13") == 0.0) {// Income Tax
			MAP_PAYSLIP_PDF.put("incomeTax", "-");
		} else {
			MAP_PAYSLIP_PDF.put("incomeTax",
					String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L13")));
		}
		if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L14") == 0.0) { // Social Security
			MAP_PAYSLIP_PDF.put("socialSecurity", "-");
		} else {
			MAP_PAYSLIP_PDF.put("socialSecurity",
					String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L14")));
		}
		if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L15") == 0.0) { // Deduct For Leave
			MAP_PAYSLIP_PDF.put("deductForLeave", "-");
		} else {
			MAP_PAYSLIP_PDF.put("deductForLeave",
					String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L15")));
		}
		if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "J15") == 0.0) { // Date Deduct For Leave
			MAP_PAYSLIP_PDF.put("dateDeductForLeave", "-");
		} else {
			MAP_PAYSLIP_PDF.put("dateDeductForLeave",
					String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "J15")));
		}
		if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L18") == 0.0) { // Deduction
			MAP_PAYSLIP_PDF.put("deduction", "-");
		} else {
			MAP_PAYSLIP_PDF.put("deduction",
					String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L18")));
		}
		// -- Deduction --
		if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F19") == 0.0) { // Net Income
			MAP_PAYSLIP_PDF.put("netIncome", "-");
		} else {
			MAP_PAYSLIP_PDF.put("netIncome",
					String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F19")));
		}
		MAP_PAYSLIP_PDF.put("approver", PlusExcelUtils.readCellAsString(sheetPaySlip, "I23")); // Approver

		return MAP_PAYSLIP_PDF;

	}

	public void convertPaySlipToPDF(UploadFilesDomain domain) throws Exception {

		for (int i = 0; i < domain.getFiles().size(); i++) {
			File fileXmls = new File(domain.getFiles().get(i).getOriginalFilename());
			if (fileXmls.toString().toLowerCase().endsWith(".xlsx") == true) {

				fileXmls.createNewFile();
				FileOutputStream fos = new FileOutputStream(fileXmls);
				fos.write(domain.getFiles().get(i).getBytes());
				fos.close();
				try {

					Workbook workbook = PlusExcelUtils.initWorkbook(fileXmls);
					Sheet sheetPaySlip = workbook.getSheet("PaySlip");
					readPaySlipPutToMap(sheetPaySlip);

					workbook.close();
					JasperReport compileReport = JasperCompileManager
							.compileReport("src/main/resources/reports/PaySlipPDF.jrxml");
					JRDataSource datasource = new JREmptyDataSource();
					JasperPrint report = JasperFillManager.fillReport(compileReport, MAP_PAYSLIP_PDF, datasource);
					byte[] data = JasperExportManager.exportReportToPdf(report);

					// folder name
					Date dateFolder = new SimpleDateFormat("dd-MMM-yy")

							.parse(PlusExcelUtils.readCellAsString(sheetPaySlip, "C8"));
					String newFolder = new SimpleDateFormat("yyyy_MM").format(dateFolder) + "_PDF";
					// file name
					String[] subNamesDate = ((String) MAP_PAYSLIP_PDF.get("date")).split("-");
					String newFile = MAP_PAYSLIP_PDF.get("employeeCode") + "_" + subNamesDate[1].toUpperCase() + "_"
							+ subNamesDate[2];
					System.out.println(outputPath + newFolder + "/" + newFile + ".pdf");
					PlusFileUtils.writeFile(outputPath + newFolder + "/" + newFile + ".pdf", data);

					Path path = Paths.get(fileXmls.getAbsolutePath());
					Files.deleteIfExists(path);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} finally {
					if (fos != null) {

						fos.close();
					}
					fos = null;
				}
			}
		}
	}
}
